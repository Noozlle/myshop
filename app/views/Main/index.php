<div class="bnr" id="home">
    <div  id="top" class="callbacks_container">
        <ul class="rslides" id="slider4">
            <li>
                <img src="images/bnr-1.jpg" alt=""/>
            </li>
            <li>
                <img src="images/bnr-2.jpg" alt=""/>
            </li>
            <li>
                <img src="images/bnr-3.jpg" alt=""/>
            </li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>

<?php  if($brands): ?>
<div class="about">
    <div class="container">
        <div class="about-top grid-1">
            <?php foreach ($brands as $brand): ?>
            <div class="col-md-4 about-left">
                <figure class="effect-bubba">
                    <img class="img-responsive" src="images/<?= !$brand->img ? 'images/watches_default' : $brand->img; ?>" alt="<?= $brand->title ?>"/>
                    <figcaption>
                        <h2><?= $brand->title ?></h2>
                        <p><?= $brand->description ?></p>
                    </figcaption>
                </figure>
            </div>
            <?php endforeach; ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php endif;
?>
<!--about-end-->
<!--product-starts-->
<?php if($watches): ?>
<div class="product">
    <div class="container">
        <div class="product-top">
            <div class="product-one">
                <?php foreach ($watches as $watch):?>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="product/<?=$watch->alias?>" class="mask"><img class="img-responsive zoom-img" src="images/<?=$watch->img?>" alt="" /></a>
                        <div class="product-bottom">
                            <h3><?= $watch->title ?></h3>
                            <p><?= $watch->description ?></p>
                            <h4><a class="addToCartLink" href="cart/add?id=<?= $watch->id ?>"><i></i></a> <span class=" item_price"><?= !empty($currency['symbol_left'] )? $currency['symbol_left'] . ' ' . round($watch->price * $currency['value'])  : round($watch->price * $currency['value'])   . ' ' . $currency['symbol_right'] ?></span></h4>
                        </div>
                        <?php if ($watch->old_price): ?>
                        <div class="srch">
                            <span><?= round((($watch->price - $watch->old_price) / $watch->price) * 100) ?>%</span>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
                <?php endforeach;?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php endif;?><!--product-end-->
<!--information-starts-->