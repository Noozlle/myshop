<?php


namespace app\widgets\menu;

use shop\App;
use shop\Cache;

class Menu{
    protected $data;
    protected $tree;
    protected $layout;
    protected $tmpl;
    protected $container = 'ul';
    protected $table = 'category';
    protected $cache = 3600*24;
    protected $cacheKey = 'shop_menu';
    protected $attrs = [];
    protected $prepend = '';

    public function __construct($options = []){
        $this->tpl = __DIR__ . '/menu_tpl/menu.php';
        $this->getOptions($options);
        dd($this->table);
        $this->run();

    }
    protected function run(){
        $cache = Cache::instance();
        $this->tmpl = $cache->get($this->cacheKey);
        if(!$this->tmpl){
            $this->data = App::$app->getProperty('cats');
            if(!$this->data){
                $this->data = \DB::getAssoc("SELECT * FROM {$this->table}");
            }
        }
            $this->output();

    }
    protected function getOptions($options){
        foreach ($options as $key=>$val){
            if(property_exists($this , $key ))
                $this->$key = $val;
        }
    }


    protected function output(){
        echo $this->tmpl;
    }

    protected function getThree(){
        $tree = [];
        $data = $this->data;
        foreach ($data as $id=>&$node){
            if(!$node['parent_id']){
                $tree[$id]= &$node;
            }else{
                $data[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
    }

    protected function getMenuHtml($tree , $delimiter = ''){
        $str = '';
        foreach ($tree as $id=>$category){
            $str .= $this->catToTemplate($category, $delimiter , $id);
        }
        return $str;
    }

    protected function catToTemplate($category , $delimiter , $id){
            ob_start();
            require $this->tpl;
            return ob_get_clean();
    }


}