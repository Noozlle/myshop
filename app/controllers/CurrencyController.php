<?php


namespace app\controllers;
use shop\App;

class CurrencyController extends AppController
{

    public function __construct($route)
    {
        parent::__construct($route);
    }

    public function changeAction(){
        $currency = !empty($_GET['curr'])?$_GET['curr']: null;
        if($currency){
            $curr = App::$app->getProperties()['currencies'][$currency];
            if(!empty($curr)){
                setcookie('currency' , $currency , time()+3600*24*7 ,'/');
            }
        }
        redirect();
    }





}