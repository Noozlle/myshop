<?php


namespace app\controllers;

use shop\App;
use shop\Cache;

class MainController extends AppController
{
    public function indexAction(){
        $brands = \DB::find('brand' , 'LIMIT 3');
        $watches = \DB::find('product' , "hit = '1' AND status = '1' LIMIT 8");
        $currency = App::$app->getProperties()['currency'];
        $this->set(compact('brands' , 'watches' , 'currency'));
    }
}