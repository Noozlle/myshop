<?php


define("DEBUG", 1);
define("ROOT", dirname(__DIR__));
define("APP", ROOT . '/app');
define("WWW", ROOT.'/public');
define("CORE", ROOT . '/vendor/shop/core');
define("LIBS", ROOT . '/vendor/shop/core/libs');
define("CACHE", ROOT . '/tmp/cache');
define("CONFIG", ROOT . '/config');
define("VIEW", 'main');
define("TMP", ROOT. '/tmp');

if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
}
else {
    $protocol = 'http://';
}
$app_path = "{$protocol}{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";
$app_path = preg_replace("#[^/]+$#" , '' , $app_path);
$app_path = str_replace('/public/', '' , $app_path);

define("PATH", $app_path);
define("ADMIN", PATH . '/admin');


require_once ROOT . '/vendor/autoload.php';