<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 09.10.2018
 * Time: 0:17
 */

namespace shop;


class ErrorHandler
{
    public function __construct()
    {
        if(DEBUG){
            error_reporting( -1);
        }else{
            error_reporting(0);
        }
        set_exception_handler([$this , 'exeptionHandler']);
    }

    public function exeptionHandler($err){

        $this->lockErr($err->getMessage(). $err->getFile(), $err->getLine());
        $this->displayErr( $err->getMessage(),  $err->getFile(), $err->getCode(), $err->getLine());
    }

    protected function lockErr($message = '', $file = '', $line = ''){
        error_log("[" . date('Y-m-d H:i:s') . "] Error: {$message} | In File: {$file} On line {$line}\n====================================\n", 3, TMP .'/error.log');
    }

    protected function displayErr($errno , $errorText, $errFile, $errLine, $res = 404){
        http_response_code($res);
        if($res == 404 && !DEBUG ){
            require WWW . '/errors/404.php';
            die();
        }
        if(DEBUG){
            require WWW . '/errors/dev.php';
        }else{
            require WWW . '/errors/prod.php';
        }
    }
}