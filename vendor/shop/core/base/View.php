<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 16.10.2018
 * Time: 12:41
 */

namespace shop\base;


use shop\App;

class View
{
    public $route;
    public $controller;
    public $view;
    public $model;
    public $prefix;
    public $data = [];
    public $meta = [];
    public $layout;

        public function __construct($route, $layout  = '', $view = '', $meta)
        {
            $this->route  = $route;
            $this->controller = $route['controller'];
            $this->model = $route['controller'];
            $this->view = $view;
            $this->prefix = $route['prefix'];
            $this->meta = $meta;
            $this->layout = $layout;
            if($layout === false){
                $this->layout = false;
            }else{
                $this->layout = $layout ?: VIEW;
            }
        }

    public function render($data){
            if(is_array($data)){
                extract($data);
            }
        $viewPath = APP . "/views/{$this->prefix}{$this->controller}/{$this->view}.php";

        if(is_file($viewPath)){
            ob_start();
            require_once $viewPath;
            $content = ob_get_clean();

        }else{
            throw new \Exception("View {$viewPath} NOT FOUND");
        }
        if($this->layout != false){
                $layoutFile = APP . "/views/Layouts/{$this->layout}.php";
                if(is_file($layoutFile)){
                    require_once $layoutFile;
                }
                else{
                    throw new \Exception("Layout {$layoutFile} not found");
                }
        }


    }

    public function getMeta(){
            $metaPath = APP . "/views/Layouts/meta.php";
            $metaObject = (object)[];
            if(empty($this->meta) || $this->meta === false){
                $metaObject->title =    App::$app->getProperty('site_name');
                $metaObject->description = 'Default Desctiprion';
                $metaObject->keywords = '1 ';
            }else{
                $metaObject = (object)$this->meta;
            }
            require_once $metaPath;

    }

}