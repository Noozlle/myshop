<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 16.10.2018
 * Time: 12:25
 */

namespace shop\base;


abstract class Controller
{
    public $route;
    public $controller;
    public $view;
    public $model;
    public $prefix;
    public $data = [];
    public $meta = [];
    public $layout;

    public function __construct($route)
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $route['action'];
        $this->prefix= $route['prefix'];
        $this->model= $route['controller'];
    }

    public function set($data){
        $this->data = $data;
    }
    public function setMeta($title = '',$description = '', $keywords = ''){
        $this->meta['title'] = $title;
        $this->meta['description'] = $description;
        $this->meta['keywords'] = $keywords;

    }

    public function getView(){
        $viewObject = new View($this->route, $this->layout, $this->view, $this->meta);
        $viewObject->render($this->data);
    }

}