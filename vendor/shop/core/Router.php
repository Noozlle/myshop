<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 16.10.2018
 * Time: 11:11
 */

namespace shop;


class Router
{
    protected  static $routes = [];
    protected  static $route = [];

    public static function add($regexp, $route = []){
            self::$routes[$regexp] = $route;
    }

    /**
     * @return array
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**
     * @return array
     */
    public static function getRoute()
    {
        return self::$route;
    }
    public static function dispatch($url){
        $url = self::removeQueryString($url);
        if (self::mathRoute($url)){
            $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'].'Controller';
            if(class_exists($controller)){
                    $controleerObject = new $controller(self::$route);
                    $action = self::lowerCamelCase(self::$route['action']) . 'Action';
                    if(method_exists($controleerObject, $action)){
                            $controleerObject->$action();
                            $controleerObject->getView();
                    }else{
                        throw new \Exception("Method $controller::$action not found");
                    }
            }else{
              throw new \Exception("Controller {$controller} not found");
            }
        }else{
           throw new \Exception( 'Page not found!' , 404);
        }
    }
    public static function mathRoute($url){
        foreach (self::$routes as $pattern => $route){
            if (preg_match("#{$pattern}#" , $url, $matches)){
                foreach ($matches as $key=>$value){
                    if (is_string($key)){
                        $route[$key] = $value;
                    }
                }
                if(empty($route['action'])){
                    $route['action'] = 'index';
                }
                if(!isset($route['prefix'])){
                    $route['prefix'] = '';
                }else{
                    $route['prefix'] .= '\\';
                }
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                return true;
            }
        }
        return false;
    }

    public static function upperCamelCase($name){
        return str_replace(' ', '',ucwords(str_replace('-', ' ', $name)));
    }

    public static function lowerCamelCase($name){
        return lcfirst(self::upperCamelCase($name) );
    }

    public static function removeQueryString($url){
        if($url){
            $params = explode('&' , $url , 2);
            if(false === strpos( $params[0], '=')){
                return rtrim($params[0] , '/');
            }else{
                return '';
            }
        }
    }

}