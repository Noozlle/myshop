<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 08.10.2018
 * Time: 23:38
 */

namespace shop;


trait TSingleton
{
    private static $instance;

    public static function instance(){
        if(self::$instance == NULL){
            self::$instance = new self;
        }
        return self::$instance;
    }


}