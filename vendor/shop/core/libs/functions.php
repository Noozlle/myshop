<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 09.10.2018
 * Time: 0:07
 */


function dd($array){
    echo '<pre>'.print_r($array, true).'</pre>';
    die();
}

function dump($array){

    echo '<pre>'.print_r($array, true).'</pre>';
}

function redirect($http = false){
    if($http){
        $redirect = $http;
    }else{
        $redirect = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']: PATH;
    }

    header("Location: $redirect");
    exit;
}