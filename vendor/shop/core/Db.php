<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 17.10.2018
 * Time: 13:54
 */

namespace shop;

class Db
{
    use TSingleton;

    protected  function __construct()
    {
        $db = require_once CONFIG .'/db_conf.php';
        class_alias('\RedBeanPHP\R', 'DB');
        \DB::setup( $db['dsn'], $db['username'], $db['password']);
        if(! \DB::testConnection()){
            throw new \Exception('Please Setup DB'  , 500);
        }
        \DB::freeze( TRUE );
        if(DEBUG){
            \DB::debug(true,1);
        }

    }
}