<?php
/**
 * Created by PhpStorm.
 * User: Myrik
 * Date: 08.10.2018
 * Time: 23:37
 */

namespace shop;


class Registry
{
    use TSingleton;
    protected static $properties = [];

    public function setProperty($key,$value){
        self::$properties[$key] = $value;
    }

    public function getProperty($key){
        if(isset(self::$properties[$key])){
            return self::$properties[$key];
        }else{
            return NULL;
        }
    }

    public function getProperties(){
        return self::$properties;
    }
}